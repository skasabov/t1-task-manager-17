package ru.t1.skasabov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
